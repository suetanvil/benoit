# Benoit Mandelbot

Benoit Mandelbot is a Mastodon bot that creates and posts high-quality
renderings of interesting sections of the
[Mandelbrot set](https://en.wikipedia.org/wiki/Mandelbrot_set).  They
can be seen [here](https://botsin.space/@benoitmandelbot).

It is written in a mixture of C and Ruby and runs on a Raspberry Pi in
my living room.


## Dependencies

* A recentish Ruby (I use 2.5.5) and the ability to install gems.
* Make and a decent C toolchain (gcc or clang are probably your best
  options).
* [libgd](https://libgd.github.io/) (available on most package
  managers)
* [ImageMagick](https://imagemagick.org)


## Setup

1. Compile the backend:

```
    cd backend
    make
    cd ..
```

2. Install the gems:

```
    cd bot
    # bundle update --bundler   # if needed
    bundle install
    cd ..
```

3. Create a work directory for the bot:

```
    cp -a workdir.example $PARENT/workdir
    cd $PARENT/workdir
    mv mandelbot.cfg.example mandelbot.cfg
```

4. Edit the config file.  In particular, you need to set the Mastodon
instance's URL and your bot's token.

5. Run the bot to confirm that it works.

```
    $CHECKOUT_DIR/benoit/bot/bot.sh
```

6. Set up a cron job or equivalent to run it at reasonable intervals.


