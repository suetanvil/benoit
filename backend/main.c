// Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
// See Copyright.txt and LICENSE.txt for terms.


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "position.h"
#include "render.h"
#include "rate.h"

// We're using Christopher Wellons' 'optparse' code.
#define OPTPARSE_IMPLEMENTATION     // Optparse code gets inlined here.
#define OPTPARSE_API static         // Make it local to this module only
#include "optparse.h"

// When rating a position, this is the maximum fraction of points
// allowed to be in the Mandelbrot Set.
#define SET_RATIO_CUTOFF 0.3

static MFloat
parse_mfloat(const char *str) {
    if (!str) {
        printf("Expecting a numeric argument here; try --help.\n");
        exit(1);
    }

    char *endptr = NULL;
    MFloat result = strtold(str, &endptr);

    if (!endptr && *endptr) {
        printf("Invalid numeric: '%s'\n", str);
        exit(1);
    }// if

    return result;
}// parse_mfloat


static int
parse_int(const char *str) {
    char *endptr = NULL;
    int result = (int)strtol(str, &endptr, 10);

    if (!endptr && *endptr) {
        printf("Invalid numeric: '%s'\n", str);
        exit(1);
    }// if

    return result;
}// parse_int

static void
printHelp(const char *argv0, struct optparse_long *opts) {
    printf("USAGE: %s [options] <x> <y> <pixel-size>\n", argv0);

    for (int n = 0; opts[n].longname; n++) {
        printf("    -%c, --%s", opts[n].shortname, opts[n].longname);
        if (opts[n].argtype != OPTPARSE_NONE) {
            printf(" <opt%s>", opts[n].argtype == OPTPARSE_REQUIRED ? "" : "?");
        }
        printf("\n");
    }
}// printHelp

static void
parse_args(char *argv[], Position *pos, int *threads) {
    static struct optparse_long longopts[] = {
        {"width",       'w',    OPTPARSE_REQUIRED},
        {"height",      'h',    OPTPARSE_REQUIRED},
        {"max-iter",    'm',    OPTPARSE_REQUIRED},
        {"output-file", 'o',    OPTPARSE_REQUIRED},
        {"rate-multi",  'l',    OPTPARSE_REQUIRED},
        {"threads",     't',    OPTPARSE_REQUIRED},

        {"cool",        'c',    OPTPARSE_NONE},
        {"warm",        'a',    OPTPARSE_NONE},
        {"rate",        'r',    OPTPARSE_NONE},
        {"centered-pos",'p',    OPTPARSE_NONE},
        {"help",        '?',    OPTPARSE_NONE},

        {0, 0, 0}
    };
    
    Bool help = FALSE;

    struct optparse options;
    optparse_init(&options, argv);

    for(;;) {
        int ch = optparse_long(&options, longopts, NULL);
        if (ch == -1) { break; }

        switch(ch) {
        case '?':
            help = TRUE;
            goto doneloop;

        case 'c':       pos->colorScheme = CS_COOL;                     break;
        case 'a':       pos->colorScheme = CS_WARM;                     break;

        case 'w':       pos->width = parse_int(options.optarg);         break;
        case 'h':       pos->height = parse_mfloat(options.optarg);     break;
        case 'm':       pos->maxItr = parse_int(options.optarg);        break;
        case 'o':       pos->filename = options.optarg;                 break;
        case 't':       *threads = parse_int(options.optarg);           break;

        case 'p':       pos->isCentered = TRUE;                         break;
        case 'r':       pos->mode = MD_RATE;                            break;

        case 'l': {
            pos->mode = MD_RATEMULTI;
            pos->rateRepeat = parse_int(options.optarg);
            break;
        }

        default:
            printf("Unknown/invalid option: %c\n", ch);
            exit(1);
        }// switch
    }
doneloop:

    if (help) {
        printHelp(argv[0], longopts);
        exit(0);
    }// if

    pos->x = parse_mfloat(optparse_arg(&options));
    pos->y = parse_mfloat(optparse_arg(&options));
    pos->pixelSize = parse_mfloat(optparse_arg(&options));

    if (optparse_arg(&options)) {
        printf("Unused trailing arguments! Try --help\n");
        exit(1);
    }// if 

    if ( pos->mode == MD_RATEMULTI && 
         (pos->width % pos->rateRepeat != 0 || 
          pos->height % pos->rateRepeat != 0)
        ) {
        printf("ERROR: width or height not divisible by rate-multi.\n");
        exit(1);
    }
}// parse_args



int
main(int argc, char *argv[]) {
    int threads = 1;
    Position pos = defaultPos();
    parse_args(argv, &pos, &threads);

    switch (pos.mode) {
    case MD_RENDER:
        printf("%dx%d%s at (%Lf, %Lf); pixel size = %Lf; escape = %u; scheme=%d"
               "\noutput = %s\n", pos.width, pos.height, 
               pos.isCentered ? " (centered)" : "", 
               pos.x, pos.y,
               pos.pixelSize, pos.maxItr, pos.colorScheme, pos.filename);
        
        if (threads < 1) {
            printf("Error: thread count must be greater than zero; got %d.\n",
                   threads);
            exit(1);
        }

        if (threads != 1) {
            printf("Using %d threads.\n", threads);
        }
        
        plotImage(&pos, threads);
        break;

    case MD_RATE: {
        double rating = ratePosition(pos, 10, SET_RATIO_CUTOFF);
        printf("#RATING: %lf\n", rating);
        break;
    }

    case MD_RATEMULTI:
        rateMulti(pos);
        break;

    default:
        return 1;       // Shouldn't happen
    }// switch 

    return 0;
}// main
