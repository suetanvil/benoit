// Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
// See Copyright.txt and LICENSE.txt for terms.


#include "mandel.h"

#include "util.h"
#include "types.h"
#include "position.h"

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <pthread.h>

static PointResult
countAt (MFloat x, MFloat y, int px, int py, MFloat pxSize,
         unsigned escape) {
    MFloat zx = 0, zy = 0;
    MFloat x0 = x + (MFloat)px*pxSize;
    MFloat y0 = y - (MFloat)py*pxSize;
    int count = 0;

    MFloat zxzx = 0;
    MFloat zyzy = 0;
    for(count = 0; count < escape; count++) {
        zxzx = zx*zx;
        zyzy = zy*zy;

        if (zxzx + zyzy >= (1<<16)) { break; }

        MFloat xtmp = zxzx - zyzy + x0;
        zy = 2*zx*zy + y0;
        zx = xtmp;
    }// for

    return (PointResult){count, zxzx + zyzy};
}// countAt


typedef struct {
    Position pos;
    int firstRow, lastRow;
    PointResult *counts;
} CountArgs;

static void
createCountsRange(CountArgs * restrict args) {
    Position pos = args->pos;
    int firstRow = args->firstRow;
    int lastRow = args->lastRow;

    assert(firstRow < pos.height && lastRow < pos.height);
        
    const int dotFreq = 1 + (pos.width * pos.height) / 100;

    int curr = firstRow * pos.width;
    for (int yy = firstRow; yy <= lastRow; yy++) {
        for (int xx = 0; xx < pos.width; xx++) {
            args->counts[curr++] = countAt(pos.x, pos.y, xx, yy, pos.pixelSize,
                                           pos.maxItr);

            if (curr % dotFreq == 0) {
                fputs(".", stdout);
                fflush(stdout);
            }// if 
        }// for
    }// for
    fputs("\n", stdout);
}// createCountsRange



PointResult *
createCounts(Position pos, int nthreads) {
    makeNonCentered(&pos);

    if (pos.height < 3 * nthreads) {
        printf("%d threads for %d rows is too low; going single-threaded.",
               nthreads, pos.height);
        nthreads = 1;
    }// if 
    
    PointResult *counts = xcalloc(pos.width * pos.height, sizeof(PointResult));
    pthread_t threadlist[nthreads];
    CountArgs args[nthreads];

    int incr = pos.height / nthreads + 1;
    for (int firstRow = 0, n = 0; n < nthreads && firstRow < pos.height; n++) {
        int lastRow = firstRow + incr;
        lastRow = lastRow < pos.height ? lastRow : pos.height - 1;

        args[n] = (CountArgs){pos, firstRow, lastRow, counts};

        if (nthreads == 1) {
            createCountsRange(&args[n]);
        } else {
            int stat = pthread_create(&threadlist[n], NULL,
                                      (void *(*)(void*))createCountsRange,
                                      (void*) &args[n]);
            if (stat) {
                printf("Error launching thread: %s\n", strerror(stat));
                exit(1);
            }// if
        }

        firstRow += incr;
    }// for 

    if (nthreads == 1) {
        // No need to join; just return.
        return counts;
    }
    
    for (int n = 0; n < nthreads; n++) {
        pthread_join(threadlist[n], NULL);
    }
    
    return counts;
}// createCounts
