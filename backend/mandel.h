// Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
// See Copyright.txt and LICENSE.txt for terms.

#ifndef MANDEL_H
#define MANDEL_H

#include "types.h"
#include "position.h"

typedef struct {
    unsigned count;
    MFloat radiusSquared;
} PointResult;

PointResult *createCounts (Position pos, int threads);

#endif
