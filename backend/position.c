// Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
// See Copyright.txt and LICENSE.txt for terms.


#include "position.h"


void makeNonCentered(Position *pos) {
    if (! pos->isCentered) return;

    pos->x -= ((MFloat)pos->width / 2)*pos->pixelSize;
    pos->y += ((MFloat)pos->height / 2)*pos->pixelSize;
    pos->isCentered = FALSE;
}// makeNonCentered

Position
defaultPos() {
    Position pos;
    pos.width = 640;
    pos.height = 400;
    pos.maxItr = 1000;
    pos.colorScheme = CS_COOL;
    pos.filename = "mandel_out.png";
    pos.isCentered = FALSE;
    pos.rateRepeat = 0;
    pos.mode = MD_RENDER;

    return pos;
}// defaultPos


Position
sectionPos(Position pos, int x, int y) {
    Position result = pos;

    result.width  = pos.width  / pos.rateRepeat;
    result.height = pos.height / pos.rateRepeat;

    result.x = pos.x + (x * result.width * pos.pixelSize);
    result.y = pos.y - (y * result.height * pos.pixelSize);

    return result;
}
