// Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
// See Copyright.txt and LICENSE.txt for terms.

#ifndef POSITION_H
#define POSITION_H

#include "types.h"

typedef enum {
    MD_RENDER,
    MD_RATE,
    MD_RATEMULTI,
} MODE;

typedef struct {
    MFloat x;                           // X (real) coordinate
    MFloat y;                           // Y (imaginary) coordinate
    MFloat pixelSize;                   // Pixel width (i.e. the zoom factor)

    int width;                          // Image width in pixels
    int height;                         // Image height in pixels
    unsigned maxItr;                    // Maximum iterations
    COLOR_SCHEME colorScheme;           // Color scheme to use

    Bool isCentered;                    // If true, x,y is image center
    MODE mode;                          // What we're actually doing
    int rateRepeat;                     // # subdivisions for MD_RATEMULTI.

    char *filename;                     // Output file or NULL
} Position;

void makeNonCentered(Position *pos);
Position defaultPos(void);
Position sectionPos(Position pos, int x, int y);


#endif
