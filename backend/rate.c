// Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
// See Copyright.txt and LICENSE.txt for terms.


#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <gd.h>

#include "util.h"
#include "render.h"
#include "mandel.h"
#include "rate.h"

#if 0
// Compute the largest difference (normalized between 0 and 1) in
// counts.
static double
biggestDiff(PointResult *counts, unsigned num_counts, int maxItr) {

    unsigned low = maxItr;
    unsigned high = 0;
    for (unsigned n = 0; n < num_counts; n++) {
        unsigned prc = counts[n].count;
        low  = prc < low  ? prc : low;
        high = prc > high ? prc : high;
    }

    return (double)(high - low) / (double)maxItr;
}// biggestDiff
#endif

// Compute the standard deviation of all of the counts.
static double
stdDeviation(PointResult *counts, unsigned num_counts, unsigned maxItr) {

    if (num_counts < 2) { return 0.0; }

    double sum = 0;
    double *normalized = xcalloc(num_counts, sizeof(double));
    for (unsigned n = 0; n < num_counts; n++) {
        double norval = (double)counts[n].count / (double)maxItr;
        normalized[n] = norval;
        sum += norval;
    }// for 

    double mean = sum / (double)num_counts;

    // Compute sum of squared differences from the mean
    double sqdiff_sum = 0;
    for (unsigned n = 0; n < num_counts; n++) {
        double diff = normalized[n] - mean;
        sqdiff_sum += diff*diff;
    }

    return sqrt(sqdiff_sum / (double)(num_counts - 1));
}// stdDeviation

// Return the fraction of point that have *not* reached the maximum
// count.
static double
ratioOutOfSet(PointResult *counts, unsigned num_counts, unsigned maxItr) {
    unsigned not_in = 0;
    for (unsigned n = 0; n < num_counts; n++) {
        if (counts[n].count < maxItr) { ++not_in; }
    }

    return (double)not_in / (double)num_counts;
}// ratioOutOfSet


static PointResult *
computeCounts(Position pos, int scale, unsigned *num_counts) {
    makeNonCentered(&pos);

    pos.width /= scale;
    pos.height /= scale;
    pos.pixelSize *= scale;

    // Let's also evaluate the bottom and right rows
    pos.width += 1;
    pos.height += 1;

    *num_counts = pos.width * pos.height;
    return createCounts(pos, 1);
}// computeCounts


// Return a rating for the position at 'pos'; if the ratio of
// out-of-set points is less than cutoff, returns zero.
double
ratePosition (Position pos, int scale, double cutoff){
    unsigned num_counts = 0;
    PointResult *counts = computeCounts(pos, scale, &num_counts);

//    double bd = biggestDiff(counts, num_counts, pos.maxItr);
    double dev = stdDeviation(counts, num_counts, pos.maxItr);
    double rset = ratioOutOfSet(counts, num_counts, pos.maxItr);

    free(counts);
    
    // Standard deviation maxes out at just over 0.5, so we double it,
    // capping at 1.0 so that it's normalized between 0 and 1 like the
    // others.
    dev = dev*2 > 1.0 ? 1.0 : dev*2;
    
    if (rset < cutoff) {
        dev = -0.00001L;
    }
    
    return dev;
}// ratePosition 


// Rate all squares in and around the Mandelbrot Set and output the
// rating as either a picture or a list of textual ratings.  This is
// used to discard known-bad areas via the --rate-multi flag.void
void
rateMulti (Position pos) {
    makeNonCentered(&pos);
    
    gdImagePtr im = NULL;
    if (*pos.filename) {
        im = gdImageCreateTrueColor(pos.width, pos.height);
    }

    for(int y = 0; y < pos.rateRepeat; y++) {
        for (int x = 0; x < pos.rateRepeat; x++) {
            Position npos = sectionPos(pos, x, y);
            double rating = ratePosition(npos, 1, 0.0);
            
            if (im) {
                int xx = x * npos.width, yy = y * npos.height;
                int tone = (int)nearbyint(255.0 * rating);
                int red = rating < 0.0000000000001 ? 128 : tone;
                
                gdImageFilledRectangle(im, xx, yy,
                                       xx + npos.width, yy + npos.height,
                                       gdImageColorExact(im, red, tone, tone));
            } else {
                printf ("#RATING_AT %lf, %Lf, %Lf, %Lf,%Lf\n", rating,
                        npos.x, npos.y,
                        npos.x + npos.width * npos.pixelSize,
                        npos.y + npos.height * npos.pixelSize);
            }
        }// for
    }// for

    if (im) {
        writeOutputFile(im, pos.filename);
    }// if 
}// rateMulti 
