// Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
// See Copyright.txt and LICENSE.txt for terms.

#ifndef RATE_H
#define RATE_H

#include "types.h"
#include "position.h"

double ratePosition (Position pos, int scale, double cutoff);
void rateMulti (Position pos);

#endif
