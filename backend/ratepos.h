// Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
// See Copyright.txt and LICENSE.txt for terms.

#ifndef RATEPOS_H
#define RATEPOS_H

#include "types.h"

// they're all good coordinates, brent!

MFloat ratePos(Position pos);

#endif
