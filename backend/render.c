// Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
// See Copyright.txt and LICENSE.txt for terms.

// Copyright 2017, Chris Reuter; NO WARRANTY; not for stealing!

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include <gd.h>

#include "util.h"
#include "mandel.h"

#include "render.h"


// Linear interpolate
static MFloat
lerp(MFloat a, MFloat b, MFloat frac) {
    return a + ((b - a) * frac);
}// lerp


static int
clrInt(MFloat c) {
    int ic = (int)round(0x100 * c);
    if (ic < 0) { return 0;}
    if (ic > 255) { return 255; }
    return ic;
}// clrInt

static int
rgb(gdImagePtr im, MFloat r, MFloat g, MFloat b) {
    return gdImageColorExact(im, clrInt(r), clrInt(g), clrInt(b));
}// rgb


struct ClrControl {
    MFloat ctrl;
    MFloat r, g, b;
};

static int
colourFor(gdImagePtr im, MFloat ratio, const struct ClrControl points[]) {
    if (ratio >= 1) { return rgb(im, 0, 0, 0); }     // The set itself is black
    if (ratio <= 0) { ratio = 0.0001; } // Should never happen w/o FP errors.

    int n = 0;
    MFloat or = 0, og = 0, ob = 0, octrl = 0;
    for ( ; points[n].ctrl < 99.0; n++) {
        if (n > 0 &&    // First iteration needed for setting o{ctrl,r,g,b}.
            ratio < points[n].ctrl) {

            MFloat frac = (ratio - octrl) / (points[n].ctrl - octrl);
            return rgb(im,
                       lerp(or, points[n].r, frac),
                       lerp(og, points[n].g, frac),
                       lerp(ob, points[n].b, frac));
        } else {
            octrl = points[n].ctrl;
            or = points[n].r;
            og = points[n].g;
            ob = points[n].b;
        }
    }

    return rgb(im, 1, 1, 1);  // Not reached!
}// colourFor


static const struct ClrControl *
getColorScheme(COLOR_SCHEME cs) {

    // SRC: https://stackoverflow.com/questions/16500656#25816111
    static const struct ClrControl WIKIPEDIA_CLR [] = {
        {0.000000, 0.000000, 0.027451, 0.392157},
        {0.160000, 0.125490, 0.419608, 0.796078},
        {0.420000, 0.929412, 1.000000, 1.000000},
        {0.642500, 1.000000, 0.666667, 0.000000},
        {0.857500, 0.000000, 0.007843, 0.000000},
        {1.000000, 0.400000, 0.400000, 1.000000},
        {999.0, 0, 0, 0}
    };

    // Source: Paul Bourke
    static const struct ClrControl CLASSIC_CLR[] = {
        {0.00, 0, 0, 1},
        {0.25, 0, 1, 1},
        {0.50, 0, 1, 0},
        {0.75, 1, 1, 0},
        {1.00, 1, 0, 0},

        {999.0, 0, 0, 0}
    };

    static const struct ClrControl *schemes[] = {
        WIKIPEDIA_CLR, CLASSIC_CLR
    };

    size_t nschemes = sizeof(schemes)/sizeof(struct ClrControl*);
    if (cs > nschemes) {
        printf("Error: only %d color schemes; you selected #%d (from zero).\n",
               (int)nschemes, cs);
        exit(1);
    }

    return schemes[cs];
}// getColorScheme





static MFloat *
createPalette (PointResult *results, const Position *pos) {

    // Create a histogram of counts
    unsigned hist[pos->maxItr + 1];
    memset(hist, 0, (pos->maxItr + 1) * sizeof(unsigned));

    const int nresults = pos->width * pos->height;
    for (int n = 0; n < nresults; n++) {
        if (results[n].count >= pos->maxItr) continue;  // members skew the results
        assert(results[n].count <= pos->maxItr);
        hist[ results[n].count ]++;
    }// for

    // Compute the total, excluding items that reached the escape
    // (i.e. are probably members of the Mandelbrot set) as they skew
    // the colouring.
    MFloat total = 0;
    for (int n = 0; n < pos->maxItr; n++) {
        total += hist[n];
    }// for

    // Now, create the palette
    MFloat hue = 0.0;
    MFloat *palette = xcalloc(pos->maxItr + 1, sizeof(MFloat));
    for (int n = 0; n < pos->maxItr; n++) {
        palette[n] = hue;
        hue += (MFloat)hist[n] / total;
    }// for

    palette[pos->maxItr] = 1;    // Set members; coloured black

    return palette;
}// createPalette


static void
colorImg (const PointResult *results, const Position *pos, 
          const MFloat *palette, gdImagePtr im) {
    const struct ClrControl *scheme = getColorScheme(pos->colorScheme);

    const MFloat log2 = log(2);
    int curr = 0;
    for (int yy = 0; yy < pos->height; yy++) {
        for(int xx = 0; xx < pos->width; xx++) {
            PointResult pr = results[curr++];

            MFloat clr = 1.0;

            if (pr.count < pos->maxItr) {
                MFloat log_zn = log(pr.radiusSquared) / 2;
                MFloat nu = log(log_zn / log2) / log2;
                MFloat fcount = (MFloat)pr.count + 1.0 - nu;
                int ifc = (int)floor(fcount);

                // Sanity check: we may have exceeded the check if
                // we've gotten too far from the set.  In that case,
                // color it white:
                if (ifc < 0 || ifc + 1 >= pos->maxItr) {
                    clr = 1.0;
                } else {
                    MFloat clr1 = palette[ ifc ];
                    MFloat clr2 = palette[ ifc + 1];
                    MFloat frac = fcount - ifc;

                    clr = lerp(clr1, clr2, frac);
                }// if .. else
            }// if

            gdImageSetPixel(im, xx, yy, colourFor(im, clr, scheme));
        }// for
    }// for
}// colorImg


void
writeOutputFile(gdImagePtr im, const char *filename) {
    assert(filename);
    FILE *out = fopen(filename, "wb");
    if (!out) {
        err(1, "Unable to open file '%s' for writing!\n", filename);
        return; // unreached.
    }// if

    gdImagePng(im, out);

    fclose(out);
}// writeOutputFile

void
plotImage (Position *pos, int threads) {
    assert(pos->filename);

    // Do the actual plotting
    PointResult *results = createCounts(*pos, threads);

    // Create the palette
    MFloat *palette = createPalette(results, pos);

    // Actually render the image onto im
    gdImagePtr im = gdImageCreateTrueColor(pos->width, pos->height);
    colorImg(results, pos, palette, im);

    // Save the picture to disk.
    writeOutputFile(im, pos->filename);

    // Cleanup
    free(results);
    free(palette);
}// plotImage 


