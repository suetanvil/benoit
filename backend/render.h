// Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
// See Copyright.txt and LICENSE.txt for terms.

#ifndef RENDER_H
#define RENDER_H

#include <gd.h>

#include "types.h"
#include "position.h"

void plotImage (Position *pos, int threads);
void writeOutputFile(gdImagePtr im, const char *filename);

#endif
