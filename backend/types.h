// Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
// See Copyright.txt and LICENSE.txt for terms.

#ifndef __TYPES_H
#define __TYPES_H

typedef long double MFloat;     // Our prefered float
typedef enum {
    CS_COOL     = 0,
    CS_WARM     = 1,
} COLOR_SCHEME;

// We define our own booleans here
#ifdef TRUE
#   undef TRUE
#endif

#ifdef FALSE
#   undef FALSE
#endif

typedef enum bool {
    FALSE = 0,
    TRUE = 1,
} Bool;


#endif
