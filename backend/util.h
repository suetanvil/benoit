// Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
// See Copyright.txt and LICENSE.txt for terms.

#ifndef UTIL_H
#define UTIL_H

#include <stdlib.h>
#include <err.h>
#include <sysexits.h>

static inline void* xcalloc(size_t count, size_t size) {
    void *result = calloc(count, size);
    if (!result) {
        errx(EX_OSERR, "calloc(%lu,%lu) failed", (unsigned long) count,
             (unsigned long) size);
    }
    return result;
}// xcalloc

#endif
