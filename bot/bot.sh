#!/bin/bash

# Launch script for the Benoit Mandelbot

set -e

botdir="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"
(cd $botdir/../backend; make)

export BUNDLE_GEMFILE="$botdir/Gemfile"
ruby -I "$botdir/lib" "$botdir/lib/main.rb" "$@"


