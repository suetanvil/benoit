#!/usr/bin/env ruby

# Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
# See Copyright.txt and LICENSE.txt for terms.

require 'rubygems'
require 'bundler/setup'

require 'yaml'
require 'set'
require 'optparse'

require 'mastodon'

require 'picker'
require 'util'

CONFIG = 'mandelbot.cfg'
RENDER_DIR = 'pending'
IMAGE = File.join(RENDER_DIR, 'toot.png')
STATUS = File.join(RENDER_DIR, 'toot.txt')


def post(url, token, dryrun)
  stat_txt = File.open(STATUS) {|fh| fh.read}
  msg "Posting status: #{stat_txt.sub("\n", "\\n")}"

  if dryrun
    msg "Dry run; not posting to #{url}."
    return
  end
  
  client = Mastodon::REST::Client.new(base_url: "https://#{url}",
                                      bearer_token: token)

  assert("#{IMAGE} not present!") { File.exist? IMAGE }
  
  msg "Uploading image #{IMAGE}"
  media = File.open(IMAGE, "rb") { |fh| client.upload_media(fh) }

  msg "Posting status"
  client.create_status(stat_txt, media_ids: [media.id])
end


def load_config
  if ! File.exist? CONFIG
    err "Can't find #{CONFIG}; quitting."
    exit 1
  end

  cfg = YAML.load( open(CONFIG, "r") { |fh| fh.read } )
  assert("Missing required fields in #{CONFIG}") {
    cfg and \
    cfg.keys.to_set == Set.new(%i{url token threshold poolsize zonemin warmrate})
  }
  return cfg
end


def render_toot(cfg, image = IMAGE, status = STATUS, addRating = false)  
  Dir.mkdir RENDER_DIR unless Dir.exist? RENDER_DIR

  if File.exist?(image) && File.exist?(status)
    msg "Found existing toot; going with that."
    return
  end

  candidate = pickone(cfg[:threshold], cfg[:poolsize], cfg[:zonemin])

  warm = rand() < cfg[:warmrate]
  msg "Rendering#{' using warm theme' if warm}."

  candidate.render(image, warm, 4)

  msg "Creating toot text."
  open(status, "w") do |fh|
    fh.puts "position: #{candidate.x} + #{candidate.y}i"
    fh.puts "pixel width: #{candidate.pixelSize}"
    fh.puts "rating: #{candidate.rating}" if addRating
  end
end

def cleanup(keep)
  if keep
    msg "Leaving post contents behind."
    return
  end

  [IMAGE, STATUS].each{|f| File.unlink f if File.exist? f}
end


# Test utility
def renderloop(cfg)
  count = 1
  while true
    ctext = sprintf("%06d", count)
    render_toot(cfg,
                File.join(RENDER_DIR, "toot.#{ctext}.png"),
                File.join(RENDER_DIR, "toot.#{ctext}.txt"),
                true)
    count += 1
  end
end


# Quick-and-dirty arg parsing
def getflags
  flags = {post: false, verbose: false, keep: false, renderloop: false}

  OptionParser.new do |opts|
    opts.on("--post", "-p", "Not a dry run; actually post.") {
      flags[:post] = true
    }

    opts.on("--verbose", "-v", "More messages") {
      flags[:verbose] = true
    }

    opts.on("--keep", "Don't delete temporary files.") {
      flags[:keep] = true
    }

    opts.on("--renderloop", "Render images until killed. TEST FEATURE.") {
      flags[:renderloop] = true
    }

    opts.on("--help", "Print this message.") {
      puts opts
      exit
    }
  end.parse!

  return flags
end

# Attempt to get a lock on the lockfile (which is truncated) and quit
# if unsuccessful.  We don't bother unlocking; that happens when the
# program exits.
def get_lock_or_exit
  fh = File.open("lockfile.txt", File::RDWR|File::CREAT, 0644)
  status = fh.flock(File::LOCK_EX|File::LOCK_NB)

  if !status 
    err "Unable to get file lock; quitting."
    exit 1
  end
 
  msg "Got file lock."
end



def main
  msg "Starting..."

  flags = getflags()
  
  setVerbose true if flags[:verbose]

  cfg = load_config()
  msg "Found config file."

  if flags[:renderloop]
    setVerbose(true)
    msg "Running in test mode renderloop"

    renderloop(cfg)
    exit 0
  end
  
  msg "Trying to get file lock."
  get_lock_or_exit()

  msg "Rendering..."
  render_toot(cfg)

  post(cfg[:url], cfg[:token], !flags[:post])

  cleanup(flags[:keep])

  return
#rescue SystemExit => e
  # We assume a message has already been logged at this point.
  exit e.status

#rescue Exception => e
  err "Fatal exception: #{e}"
  exit 1
end

main()

