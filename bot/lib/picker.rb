# Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
# See Copyright.txt and LICENSE.txt for terms.

require 'position.rb'

def pickmany(count, threshold, zoneMin)
  pns = (1..count).collect { Position.random(zoneMin) }

  pns.each{|p| p.rating}      # Wait until all are processed

  good = pns.select {|p| p.rating >= threshold}

  msg "selected #{good.size} of #{count}"

  return good
end


def pickone(threshold, poolsize, zoneMin)
  pool = []
  while pool.size < poolsize
    candidates = pickmany([20, poolsize].max, threshold, zoneMin)
    pool += candidates
    msg "Found #{candidates.size}; got #{pool.size} of #{poolsize}"
  end

  candidates = pool.sort{|a, b| a.rating <=> b.rating}
  candidates.each {|c| msg " ->#{c} #{c.rating}"}
  return candidates.last
end

