# Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
# See Copyright.txt and LICENSE.txt for terms.

require 'simple-future'

require 'util'
require 'ratedrect'



CMANDEL = cmandel_path()
WIDTH = 800
HEIGHT = 600
MAXITER = 10000


class Position
  attr_reader :x, :y, :pixelSize

  def initialize(x, y, pixelSize)
    @x = x
    @y = y
    @pixelSize = pixelSize

    @rating = SimpleFuture.new { computeRating }
  end

  def rating
    return @rating.value
  end

  def haveRating?
    return @rating.complete?
  end

  def render(filename, warm = false, scale = 1)
    # Shortcut
    return cmandel(false, filename, warm, WIDTH, HEIGHT) if scale == 1

    tmpfile = File.join(File.dirname(filename),
                        "tmp.#{Process.pid}.#{rand(9999)}.png")
    cmandel(false, tmpfile, warm, WIDTH, HEIGHT, scale)
    assert("Error running 'convert'.") {
      system("convert", tmpfile, "-scale", "#{100/scale}%", filename)
    }

    File.unlink(tmpfile)
  end
  
  def self.trueRandom(maxPixelSize = 0.000375)
    x = 3*rand() - 2
    y = 2.4*rand() - 1.2
    ps = randomPixelSize(maxPixelSize)

    return Position.new(x, y, ps)
  end

  def self.random(minRating = 0.0)
    bounds = RatedRect.ratings.select {|r| r.rating >= minRating}.sample
    raise "Unable to find suitable rated rectangle" unless bounds
    
    maxPixelSize = 3.0 / WIDTH   #4 * (bounds.brx - bounds.tlx) / WIDTH

    x = range(bounds.tlx, bounds.brx)
    y = range(bounds.tly, bounds.bry)
    ps = self.randomPixelSize(maxPixelSize)
    
    return Position.new(x, y, ps)
    
  end
  
  def to_s
    return "Position.new(#{@x},#{@y},#{@pixelSize})"
  end
  
  private

  def cmandel(rate, output, warm, width, height, scale = 1)
    cmd = "#{CMANDEL} -w #{width*scale} -h #{height*scale} -m #{MAXITER}"
    cmd += " -r -o ''"      if rate
    cmd += " -o #{output}"  unless rate
    cmd += " --warm"        if warm
    
    cmd += " -- #{@x} #{@y} #{@pixelSize/scale}"

    result = `#{cmd}`.split(/\n/)
    assert("Error running backend!") { $?.exitstatus == 0 }
    return result
  end

  def computeRating
    lines = cmandel(true, "", false, WIDTH, HEIGHT)
    lines = lines.select {|l| l =~ /^#RATING:/ }

    assert("Expecting exactly one #RATING line!") { lines.size == 1 }

    m = /^#RATING:\s+(-?\d+\.\d+)/.match(lines[0])
    assert("Malformed RATING line: '#{lines[0]}'") { m }
    
    return m[1].to_f
  end


  # Return a random pixel size of less than maxPixelSize, scaled so
  # that all valid orders of magnitude are equally possible.
  def self.randomPixelSize(maxPixelSize)
    ps = maxPixelSize * rand()

    minScale = Math.log10(maxPixelSize).abs
    scale = Math.log10(Float::EPSILON).abs - minScale
    exp = rand(scale.abs.floor - 1) + minScale
    ps *= 10 ** -exp

    return ps
  end
end



