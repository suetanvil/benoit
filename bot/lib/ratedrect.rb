# Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
# See Copyright.txt and LICENSE.txt for terms.

require 'util'

backend_build()

class RatedRect
  attr_reader :rating, :tlx, :tly, :brx, :bry
  
  def initialize(rating, tlx, tly, brx, bry)
    @rating = rating
    @tlx = tlx
    @tly = tly
    @brx = brx
    @bry = bry
  end

  def to_s
    "RatedRect.new(#{@rating},#{@tlx},#{@tly},#{@brx},#{@bry})"
  end
  
  @@ratings = nil
  def self.ratings
    return @@ratings if @@ratings

    @@ratings = []
    IO.foreach(File.join(rootdir, "backend", "ratings.txt")) do |line|
      next unless /^#RATING_AT (?<values>.+)$/ =~ line
      rating, tlx, tly, brx, bry = values.split(/,\s*/).collect{|s| s.to_f}
      next unless rating > 0.0
      @@ratings.push RatedRect.new(rating, tlx, tly, brx, bry)
    end
    
    return @@ratings
  end
end


