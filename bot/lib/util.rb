# Benoit Mandelbot, Copyright (C) 2017-2021, Chris Reuter; AGPLv3; NO WARRANTY!
# See Copyright.txt and LICENSE.txt for terms.

require 'logger'

class AssertionError < RuntimeError; end

def assert (*args)
  return if yield
  assertfail(*args)
end

# Raise an assertion error
def assertfail(*args)
  msg = args.map{|s|s.to_s}.join(" ")
  raise AssertionError.new(msg)
end

def rootdir
    here = __FILE__
    here = File.readlink(here) while File.lstat(here).symlink?
    return File.absolute_path(File.join(File.dirname(here), "..", ".."))
end

def cmandel_path
  return File.join(rootdir, 'backend', 'cmandel')
end

def backend_build
  out = `cd #{rootdir()}/backend; make ratings.txt`
  raise "Compile error: #{out}" if $?.exitstatus != 0
end

def range(low, high)
  low, high = [high, low] if low > high
  return low + (high - low)*rand()
end
  
module Messages
  @@verbose = false
  @@logger = Logger.new('mandelbot.log')

  def setVerbose(v)
    @@verbose = !!v
  end

  def verbose?
    @@verbose
  end
  
  def msg(*args)
    line = args.join(" ")
    @@logger.info(args)
    puts line if @@verbose
  end

  def err(*args)
    line = args.join(" ")
    puts "ERROR: #{line}"
    @@logger.error(line)
  end
end

include Messages
